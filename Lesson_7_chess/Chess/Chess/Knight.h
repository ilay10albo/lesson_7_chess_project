#pragma once
#include "Tool.h"
#include "Point.h"

class Knight :
	public Tool
{
public:
	virtual int move(Point src, Point dest, Board* b);
	Knight(char type);
	virtual ~Knight();
};

