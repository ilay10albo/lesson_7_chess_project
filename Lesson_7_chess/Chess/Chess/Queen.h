#pragma once
#include "Tool.h"
class Queen :
	public Tool
{
public:
	virtual int move(Point src, Point dest, Board* b);
	Queen(char type);
	virtual ~Queen();
};

