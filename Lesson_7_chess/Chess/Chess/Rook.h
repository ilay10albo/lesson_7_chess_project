#pragma once
#include "Tool.h"
#include "Point.h"

class Rook :
	public Tool
{
public:
	virtual int move(Point src, Point dest, Board* b);
	Rook(char type);
	virtual ~Rook();
};

