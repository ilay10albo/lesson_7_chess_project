#pragma once
#include "Point.h"
#include "Tool.h"
class King :
	public Tool
{
public:
	virtual int move(Point src, Point dest, Board* b);
	King(char type);
	virtual ~King();
};

