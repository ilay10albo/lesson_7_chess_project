#pragma once
#include "Tool.h"
class Pawn :
	public Tool
{
public:
	virtual int move(Point src, Point dest, Board* b);
	Pawn(char type);
	virtual ~Pawn();
};

