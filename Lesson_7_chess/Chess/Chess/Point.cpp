#include "Point.h"



double Point::avgPoint()
{
	return (((double)this->_x + this->_y) / 2);
}

bool Point::operator==(Point p)
{
	return (this->getX() == p.getX() && this->getY() == p.getY());
}

bool Point::operator!=(Point p)
{
	return (this->getX() != p.getX() || this->getY() != p.getY());
}

bool Point::operator>=(Point p)
{
	return (this->avgPoint() >= p.avgPoint());
}

bool Point::operator<=(Point p)
{
	return (this->avgPoint() <= p.avgPoint());
}

bool Point::operator>(Point p)
{
	return (this->avgPoint() > p.avgPoint());
}

bool Point::operator<(Point p)
{
	return (this->avgPoint() < p.avgPoint());
}

Point::Point(char x, char y)
{
	
	this->_x = x - '0' - 1;
	this->_y = y - 'a';
	
}

Point::Point(int x, int y)
{
	_x = x;
	_y = y;
}

Point::Point()
{
	this->_x = 0;
	this->_y = 0;
}

int Point::getX()
{
	return this->_x;
}

int Point::getY()
{
	return this->_y;
}


Point::~Point()
{
}

void Point::PrintPoint()
{
	printf("X is a row index, Y is a column index\n");
	cout << "X: " << this->_x << " Y: " << this->_y << "\n\n";
}
