#include "Tool.h"

char Tool::getType() const
{
	return this->_type;
}

bool Tool::isBlackCheck() const
{
	return isupper(this->_type);
}

Tool::Tool(char type)
{
	this->_type = type;
}

Tool::~Tool()
{

}
