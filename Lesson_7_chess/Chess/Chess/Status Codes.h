#pragma once
//This headers is for the statuses that we need 
//to send them to the front.
//Constants are more professionals.

#define VALID_MOVE 0 //Valid move - without something special.
#define CHESS_MOVE 1 //A move that caused to a chess to the other player.
#define NOT_FOUND_PLAYER_TOOL_IN_SRC 2 //Do not have a current player's tool on the source point. 
#define CURR_PLAYER_IN_DEST 3//Current player's tool is on the dest point of the move.
#define SELF_CHESS_MOVE 4 //If as a result from the move will be a self chess on the player.
#define INVALID_POINT_INDEXCES 5 //If one of the points (src or dest) is invalid point on the board.
#define INVALID_TOOL_MOVE 6 //If the players tried to move a tool in an invalid way. 
#define SAME_DST_AND_SRC 7 //The src point and the dst point are the same point.
#define CHESSMATE_MOVE 8 //As a result from the move the player did a chess mate on the other player. 