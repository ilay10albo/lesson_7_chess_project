#include "Pipe.h"
#include "Tool.h"
#include <iostream>
#include <thread>
#include <string>
#include "Point.h"
#include "Board.h"
#include "Bishop.h"

using std::cout;
using std::endl;
using namespace std;


int main()
{

	Board b(INIT_BOARD_MSG);
	Bishop bs('r');
	b.printBoard();
	bs.move(Point(1,2), Point(2,3), &b);

	getchar();
	//srand(time_t(NULL));
	//Pipe p;


	//bool isConnect = p.connect();
	//string ans;
	//while (!isConnect)
	//{
	//	cout << "cant connect to graphics" << endl;
	//	cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
	//	cin >> ans;

	//	if (ans == "0")
	//	{
	//		cout << "trying connect again.." << endl;
	//		Sleep(5000);
	//		isConnect = p.connect();
	//	}
	//	else 
	//	{
	//		p.close();
	//		return 0;
	//	}
	//}
	//
	//
	//// Connection is astablished ... 

	//char msgToGraphics[1024];
	//// msgToGraphics should contain the board string accord the protocol
	//// YOUR CODE
	//strcpy_s(msgToGraphics, INIT_BOARD_MSG); // just example...
	//
	//p.sendMessageToGraphics(msgToGraphics);   // send the board string

	//// get message from graphics
	//string msgFromGraphics = p.getMessageFromGraphics();

	//while (msgFromGraphics != "quit")
	//{
	//	// should handle the string the sent from graphics
	//	// according the protocol. Ex: e2e4           (move e2 to e4)
	//	
	//	// YOUR CODE
	//	strcpy_s(msgToGraphics, "0"); // msgToGraphics should contain the result of the operation

	//	// return result to graphics		
	//	p.sendMessageToGraphics(msgToGraphics);   

	//	// get message from graphics
	//	msgFromGraphics = p.getMessageFromGraphics(); // String e4 e9

	//	Point a(msgFromGraphics[1], msgFromGraphics[0]);
	//	Point b(msgFromGraphics[3], msgFromGraphics[2]);
	//	a.PrintPoint();
	//	b.PrintPoint();

	//}

	//p.close();
	return 0;
}
